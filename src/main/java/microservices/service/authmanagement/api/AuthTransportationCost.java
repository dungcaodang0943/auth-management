package microservices.service.authmanagement.api;

import lombok.extern.log4j.Log4j2;
import microservices.service.authmanagement.entity.WarehouseEntity;
import microservices.service.authmanagement.payload.request.GetTransportationCostRequest;
import microservices.service.authmanagement.payload.response.MessageResponse;
import microservices.service.authmanagement.payload.response.TransportationCostResponse;
import microservices.service.authmanagement.repository.WarehouseRepository;
import microservices.service.authmanagement.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

@Log4j2
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/")
public class AuthTransportationCost {

    @Autowired
    WarehouseRepository warehouseRepository;

    private double priceDefault = 16000; //Price is VND
    private double weightDefalut = 3;    // Weigth is kg
    private double priceIncease = 4000;   //Price is VND

    @PostMapping("get-transportation-cost")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getTransportationCost(@RequestBody GetTransportationCostRequest getTransportationCostRequest) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String addressSender;
        String addressSenderCode;
        if (principal instanceof UserDetails) {
            addressSender = ((UserDetailsImpl) principal).getAddress();
            addressSenderCode = ((UserDetailsImpl) principal).getProvinceCode();
        } else {
            return ResponseEntity.ok((new MessageResponse("Invalid JWT token")));
        }

        long warehouseReceiveId;
        long warehouseDeliveryId;
        int warehouseReceive = getTransportationCostRequest.getIsDirectlySend();
        int warehouseDelivery = getTransportationCostRequest.getIsDirectlyReceive();
        double weight = getTransportationCostRequest.getWeight();

//        String addressSender = getTransportationCostRequest.getAddressSender();
        String addressReceiver = getTransportationCostRequest.getAddressReceiver();
//        String addressSenderCode = getTransportationCostRequest.getAddressSenderCode();
        String addressReceiverCode = getTransportationCostRequest.getAddressReceiverCode();
        String addressWarehouseReceive = getAddressWarehouseReceiveBaseAddressSender(addressSenderCode, addressSender);
        String addressWarehouseDelivery = getAddressWarehouseDeliveryBaseAddressReceiver(addressReceiverCode, addressReceiver);

        String[] addressWarehouseReceiveArr = addressWarehouseReceive.split("\\|");
        String[] addressWarehouseDeliveryArr = addressWarehouseDelivery.split("\\|");

        warehouseReceiveId = Long.parseLong(addressWarehouseReceiveArr[0]);
        addressWarehouseReceive = addressWarehouseReceiveArr[1];
        warehouseDeliveryId = Long.parseLong(addressWarehouseDeliveryArr[0]);
        addressWarehouseDelivery = addressWarehouseDeliveryArr[1];

        System.out.println("okkkk " + addressWarehouseReceive + " " + addressWarehouseDelivery);

        double distanceBetweenSenderAndWarehouseReceive = 0;
        double distanceBetweenReceiverAndWarehouseDelivery = 0;
        double distanceBetweenWarehouseReceiveAndWarehouseDelivery = 0;
        double totalDistance = 0;
        double transportationCost = 0;

        if (warehouseReceive == 0) {
            distanceBetweenSenderAndWarehouseReceive = getDistanceBetweenTwoAddress(addressSender, addressWarehouseReceive);
        }

        if (warehouseDelivery == 0) {
            distanceBetweenReceiverAndWarehouseDelivery = getDistanceBetweenTwoAddress(addressReceiver, addressWarehouseDelivery);
        }

        distanceBetweenWarehouseReceiveAndWarehouseDelivery = getDistanceBetweenTwoAddress(addressWarehouseReceive, addressWarehouseDelivery);
        totalDistance = distanceBetweenSenderAndWarehouseReceive + distanceBetweenReceiverAndWarehouseDelivery + distanceBetweenWarehouseReceiveAndWarehouseDelivery;

        transportationCost = caculateTransportationCost(weight, totalDistance);
        log.info("getTransportationCost susscessfully");
        WarehouseEntity warehouseEntity;
        warehouseEntity = warehouseRepository.getWarehouse(warehouseReceiveId);
        String addressSrcWarehouse = warehouseEntity.getAddress();
        warehouseEntity = warehouseRepository.getWarehouse(warehouseDeliveryId);
        String addressDestWarehouse = warehouseEntity.getAddress();

        return ResponseEntity.ok(new TransportationCostResponse(String.format("%,.0f", transportationCost), warehouseReceiveId, warehouseDeliveryId, addressSrcWarehouse, addressDestWarehouse,"Get transportation cost successfully!"));
    }

    public double caculateTransportationCost(double weight, double distance) {

        distance = distance / 1000;

        double price = getPriceBaseWeight(weight);
        double discount = getDiscountBaseDistance(distance);

        distance = distance / 1000;

        double cost = price * distance;

        return cost;
    }

    public double getDistanceBetweenTwoAddress(String origin, String destination) {
        double distance = 0;
        try {

//            origin = "63 Thân Nhân Trung, Phường 13, Tân Bình, Thành phố Hồ Chí Minh";
//            destination = "211 Lạc Long Quân, Phường Nghĩa Đô, Quận Cầu Giấy, Hà Nội";
//            String API_KEY = "a2N2zFTfsWpZ0Qh20YGTcvejH4KV8";
            String API_KEY = "AIzaSyCLC8Dw7wItISMh9A_m34OtUFQt2hD3IB8";
//            String API_KEY = "AIzaSyAGbWF1KQ43PItIxxTtyigHnPuqwNh71O8";
            origin = encodeUrl(origin);
            destination = encodeUrl(destination);
//            String urlAddress = "https://api.distancematrix.ai/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destination + "&key=" + API_KEY;
            String urlAddress = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destination + "&key=" + API_KEY;
//            System.out.println(urlAddress);

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            String line, outputString = "";
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
//            String json = '{"destination_addresses":["211 Đ. Lạc Long Quân, Nghĩa Đô, Cầu Giấy, Hà Nội, Vietnam"],"origin_addresses":["63 Thân Nhân Trung, Phường 13, Tân Bình, Thành phố Hồ Chí Minh, Vietnam"],"rows":[{"elements":[{"distance":{"text":"1497.7 km","value":1497731},"duration":{"text":"27 hr","value":97139},"status":"OK"}]}],"status":"OK"}';
            JSONObject obj = new JSONObject(outputString);

            JSONArray arr = obj.getJSONArray("rows");
            for (int i = 0; i < arr.length(); i++) {
                JSONArray elements = arr.getJSONObject(i).getJSONArray("elements");
                for (int j = 0; j < elements.length(); j++) {
                    distance = elements.getJSONObject(i).getJSONObject("distance").getInt("value");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("getDistanceBetweenTwoAddress susscessfully");
        return distance;
    }

    public String encodeUrl(String strEncode) {
        try {
            strEncode = URLEncoder.encode(strEncode, "UTF-8")
                    .replaceAll("\\+", "%20");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strEncode;
    }

    public String getAddressWarehouseReceiveBaseAddressSender(String addressSenderCode, String addressSender) {
//        String addressWarehouseReceive = "63 Thân Nhân Trung, Phường 13, Tân Bình, Thành phố Hồ Chí Minh";
        String addressWarehouseReceive;
        List<WarehouseEntity> warehouseEntityList = warehouseRepository.getListAddressWarehouse(addressSenderCode);

        String addressWarehouse;
        double distance;
        Map<Double, String> hashMap = new HashMap();
        for (WarehouseEntity row : warehouseEntityList) {
            addressWarehouse = row.getAddress();
            distance = getDistanceBetweenTwoAddress(addressSender, addressWarehouse);
            hashMap.put(new Double(distance), row.getId().toString() + "|" + addressWarehouse);
        }

        distance = Collections.min(hashMap.keySet());
        addressWarehouseReceive = hashMap.get(distance);
        log.info("getAddressWarehouseReceiveBaseAddressSender successfully!");
        return addressWarehouseReceive;
    }

    public String getAddressWarehouseDeliveryBaseAddressReceiver(String addressReceiverCode, String addressReceiver) {
//        String addressWarehouseDelivery = "211 Lạc Long Quân, Phường Nghĩa Đô, Quận Cầu Giấy, Hà Nội";
        String addressWarehouseDelivery;
        List<WarehouseEntity> warehouseEntityList = warehouseRepository.getListAddressWarehouse(addressReceiverCode);

        String addressWarehouse;
        double distance;
        Map<Double, String> hashMap = new HashMap();
        for (WarehouseEntity row : warehouseEntityList) {
            addressWarehouse = row.getAddress();
            distance = getDistanceBetweenTwoAddress(addressReceiver, addressWarehouse);
            hashMap.put(new Double(distance), row.getId().toString() + "|" + addressWarehouse);
        }

        distance = Collections.min(hashMap.keySet());
        addressWarehouseDelivery = hashMap.get(distance);

        log.info("getAddressWarehouseDeliveryBaseAddressReceiver successfully!");
        return addressWarehouseDelivery;
    }

    public double getPriceBaseWeight(double weight) {
        double price = priceDefault;
        if (weight > weightDefalut) {
            weight = weight - weightDefalut;
            price += (weight/0.5) * priceIncease;
        }
        log.info("getPriceBaseWeight susscessfully");
        return price;
    }

    public double getDiscountBaseDistance(double distance) {
        double discount = 0;
        if (distance > 0 && distance < 250) {
            discount = 0;
        } else if (distance >= 250 && distance < 500) {
            discount = 0.02;
        } else if (distance >= 500 && distance < 1000) {
            discount = 0.05;
        } else if (distance >= 1000 && distance < 2000) {
            discount = 0.08;
        } else if (distance >= 2000 && distance < 3000) {
            discount = 0.1;
        } else if (distance >= 3000) {
            discount = 0.15;
        }else if(distance<0){
            discount=1;
        }
        return discount;
    }
}
