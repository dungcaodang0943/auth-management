package microservices.service.authmanagement.api;

import lombok.extern.log4j.Log4j2;
import microservices.service.authmanagement.entity.*;
import microservices.service.authmanagement.payload.request.AddressWarehouseRequest;
import microservices.service.authmanagement.payload.request.UserUpdateInfoRequest;
import microservices.service.authmanagement.payload.request.VehicleRequest;
import microservices.service.authmanagement.payload.response.*;
import microservices.service.authmanagement.repository.DriverRepository;
import microservices.service.authmanagement.repository.StockerRepository;
import microservices.service.authmanagement.repository.UserRepository;
import microservices.service.authmanagement.repository.WarehouseRepository;
import microservices.service.authmanagement.service.UserDetailsImpl;
import microservices.service.authmanagement.utils.GenID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Log4j2
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/crud")
public class AuthCrud {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    WarehouseRepository warehouseRepository;

    @Autowired
    StockerRepository stockerRepository;

    @Autowired
    EntityManager entityManager;

    @PutMapping("/update-info-user")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateInfoUser(@Valid @RequestBody UserUpdateInfoRequest userUpdateInfoRequest) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId;
        if (principal instanceof UserDetails) {
            userId = ((UserDetailsImpl) principal).getId();
        } else {
            return ResponseEntity.ok((new MessageResponse("Invalid JWT token")));
        }
        userRepository.updateUserInfo(userId,
                userUpdateInfoRequest.getEmail(),
                userUpdateInfoRequest.getPhone(),
                userUpdateInfoRequest.getUsername(),
                userUpdateInfoRequest.getAddress(),
                userUpdateInfoRequest.getProvinceCode());
        return ResponseEntity.ok(new MessageResponse("Update user's info successfully!"));
    }
    @PutMapping("/change-password-user")
//    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> changePasswordUser(@RequestBody Map<String, String> requestParams) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId;
        String username;
        String oldPassword = requestParams.get("oldPassword");
        String newPassword = requestParams.get("newPassword");
        if (principal instanceof UserDetails) {
            userId = ((UserDetailsImpl) principal).getId();
            username = ((UserDetailsImpl) principal).getPhone();
        } else {
            return ResponseEntity.ok((new MessageResponse("Invalid JWT token")));
        }
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, oldPassword));
        } catch (Exception e) {
            return ResponseEntity.ok(new MessageResponse("Password is not right!"));
        }

        userRepository.changeUserPassword(userId, encoder.encode(newPassword));

        return ResponseEntity.ok(new MessageResponse("Change user's password successfully!"));
    }

    @PutMapping("/update-status-driver")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateStatusDriver(@RequestBody Map<String, Integer> requestParams) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId;
        if (principal instanceof UserDetails) {
            userId = ((UserDetailsImpl) principal).getId();
        } else {
            return ResponseEntity.ok((new MessageResponse("Invalid JWT token")));
        }
        driverRepository.updateStatusDriver(userId,
                requestParams.get("status"));
        return ResponseEntity.ok(new MessageResponse("Update driver's status successfully!"));
    }

    @PostMapping("/add-address-warehouse")
    public ResponseEntity<?> addAddressWarehouse(@Valid @RequestBody AddressWarehouseRequest addressWarehouseRequest) {

        Long uniqueId = GenID.generateUniqueId();

        WarehouseEntity warehouseEntity = new WarehouseEntity(uniqueId, addressWarehouseRequest.getAreaCode(), addressWarehouseRequest.getAddress());

        warehouseRepository.save(warehouseEntity);
        return ResponseEntity.ok((new MessageResponse("Add address warehouse successfully!")));
    }

    @GetMapping("/get-driver-each-warehouse")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getDriverEachWarehouse() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        long userId;
        String userRole;

        if (principal instanceof UserDetails) {
            userId = ((UserDetailsImpl) principal).getId();
            userRole = ((UserDetailsImpl) principal).getRole();
        } else {
            return ResponseEntity.ok((new MessageResponse("Invalid JWT token")));
        }
        log.info("getDriverEachWarehouse: " + userId + " " + userRole);
        if (userRole.equals("ROLE_STOCKER")) {
            TypedQuery<Object[]> query
                    = entityManager.createQuery(
                    "select u.username, u.phone, d.id, d.area, d.vehicleId, d.status from UserEntity u, DriverEntity d, StockerEntity s where u.id = d.id and d.area = s.areaWarehouseId and s.id = :id", Object[].class);
            query.setParameter("id", userId);
            List<Object[]> resultList = query.getResultList();

            List<DriverWarehouseResponse> drivers = new ArrayList<>();

            for (Object[] row : resultList) {
                DriverEntity driverEntity = new DriverEntity();
                DriverWarehouseResponse driverWarehouseResponse = new DriverWarehouseResponse();
                driverWarehouseResponse.setUsername((String) row[0]);
                driverWarehouseResponse.setPhone((String) row[1]);
                driverEntity.setId((long) row[2]);
                driverEntity.setArea((long) row[3]);
                driverEntity.setVehicleId((long) row[4]);
                driverEntity.setStatus((int) row[5]);
                driverWarehouseResponse.setDriverEntity(driverEntity);
                drivers.add(driverWarehouseResponse);
            }
            log.info("List driver each warehouse: ");
            if (resultList != null) {
                return ResponseEntity.ok(drivers);
            } else {
                return ResponseEntity.ok(new MessageResponse("Don't have driver from warehouse address"));
            }
        }
        else {
            return ResponseEntity.ok(new MessageResponse("Not stoker"));
        }
    }

    @GetMapping("/get-list-driver")
    @PreAuthorize("isAuthenticated()")
    public List<ListDriverResponse> getListDriver() {

        TypedQuery<Object[]> query
                = entityManager.createQuery(
                "SELECT u.id, u.username, u.role, v.id, v.currentWeight, v.totalWeight, w.id, w.address, w.areaCode, u.email, u.phone FROM UserEntity u, DriverEntity d, VehicleEntity v, WarehouseEntity w WHERE u.id = d.id and d.vehicleId = v.id and d.area = w.id", Object[].class);
        List<Object[]> resultList = query.getResultList();

        List<ListDriverResponse> drivers = new ArrayList<>();

        for (Object[] row : resultList) {
            ListDriverResponse listDriverResponse = new ListDriverResponse();
            VehicleResponse vehicleResponse = new VehicleResponse();
            WarehouseResponse warehouseResponse = new WarehouseResponse();

            listDriverResponse.setId((long) row[0]);
            listDriverResponse.setUsername((String) row[1]);
            listDriverResponse.setRole((String) row[2]);
            listDriverResponse.setEmail((String) row[9]);
            listDriverResponse.setPhone((String) row[10]);

            vehicleResponse.setId((long) row[3]);
            vehicleResponse.setCurrentWeight((float) row[4]);
            vehicleResponse.setTotalWeight((float) row[5]);

            warehouseResponse.setId((long) row[6]);
            warehouseResponse.setAddress((String) row[7]);
            warehouseResponse.setAreaCode((String) row[8]);

            listDriverResponse.setVehicleResponse(vehicleResponse);
            listDriverResponse.setWarehouseResponse(warehouseResponse);
            drivers.add(listDriverResponse);
        }
        log.info("Get list driver for admin successfully!: ");
        return drivers;
    }

    @GetMapping("/get-vehicle-unused")
    @PreAuthorize("isAuthenticated()")
    public List<VehicleResponse> getVehicleUnused() {

        TypedQuery<Object[]> query
                = entityManager.createQuery(
                "SELECT v.id, v.currentWeight, v.totalWeight FROM VehicleEntity v WHERE v.id NOT IN (SELECT d.vehicleId FROM DriverEntity d)", Object[].class);
        List<Object[]> resultList = query.getResultList();

        List<VehicleResponse> vehicles = new ArrayList<>();

        for (Object[] row : resultList) {
            VehicleResponse vehicleResponse = new VehicleResponse();

            vehicleResponse.setId((long) row[0]);
            vehicleResponse.setCurrentWeight((float) row[1]);
            vehicleResponse.setTotalWeight((float) row[2]);

            vehicles.add(vehicleResponse);
        }
        log.info("Get vehicle unused for admin successfully!: ");
        return vehicles;
    }

    @GetMapping("/get-list-stocker")
    @PreAuthorize("isAuthenticated()")
    public List<ListStockerResponse> getListStocker() {

        TypedQuery<Object[]> query
                = entityManager.createQuery(
                "SELECT s.id, w.id, w.address, w.areaCode, u.email, u.username, u.role FROM UserEntity u, StockerEntity s, WarehouseEntity w WHERE u.id = s.id and s.areaWarehouseId = w.id", Object[].class);
        List<Object[]> resultList = query.getResultList();

        List<ListStockerResponse> stockers = new ArrayList<>();

        for (Object[] row : resultList) {
            ListStockerResponse listStockerResponse = new ListStockerResponse();
            WarehouseResponse warehouseResponse = new WarehouseResponse();

            listStockerResponse.setId((long) row[0]);
            listStockerResponse.setUsername((String) row[5]);
            listStockerResponse.setRole((String) row[6]);
            listStockerResponse.setEmail((String) row[4]);

            warehouseResponse.setId((long) row[1]);
            warehouseResponse.setAddress((String) row[2]);
            warehouseResponse.setAreaCode((String) row[3]);

            listStockerResponse.setWarehouseResponse(warehouseResponse);
            stockers.add(listStockerResponse);
        }
        log.info("Get list stocker for admin successfully!: ");
        return stockers;
    }

    @GetMapping("/get-list-warehouse")
    @PreAuthorize("isAuthenticated()")
    public List<WarehouseEntity> getListWarehouse() {

        List<WarehouseEntity> warehouses = warehouseRepository.getListWarehouse();

        log.info("Get list warehouse for admin successfully!: ");
        return warehouses;
    }
}
