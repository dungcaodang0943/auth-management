package microservices.service.authmanagement.api;


import lombok.extern.log4j.Log4j2;
import microservices.service.authmanagement.entity.*;
import microservices.service.authmanagement.payload.request.*;
import microservices.service.authmanagement.payload.response.*;
import microservices.service.authmanagement.repository.*;
import microservices.service.authmanagement.security.jwt.JwtUtil;
import microservices.service.authmanagement.service.UserDetailsImpl;
import microservices.service.authmanagement.utils.GenID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.Valid;

@Log4j2
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthApi {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    StockerRepository stockerRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    EntityManager entityManager;

    @Autowired
    WarehouseRepository warehouseRepository;

    @PostMapping("/sign-in")
    public ResponseEntity<?> authenticateUser(@RequestBody SignInRequest signInRequest) {

        System.out.println(signInRequest.getUsername() + " "+ signInRequest.getPassword());
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtil.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            Long userId = userDetails.getId();
            String username = userDetails.getUsername();
            String userEmail = userDetails.getEmail();
            String userPhone = userDetails.getPhone();
            String userAddress = userDetails.getAddress();
            String userRole = userDetails.getRole();
            String userProvinceCode = userDetails.getProvinceCode();

            JwtResponse jwtResponse = new JwtResponse(userId,
                    username,
                    userEmail,
                    userPhone,
                    userAddress,
                    jwt,
                    userRole,
                    userProvinceCode);

            if (userRole.equals("ROLE_DRIVER_INTER") || userRole.equals("ROLE_DRIVER_EXTER")) {
                Long driverId = userDetails.getId();
                DriverEntity driverEntity = driverRepository.getDriver(driverId);
                VehicleEntity vehicleEntity = driverRepository.getVehicle(driverId);

                int statusDriver = driverEntity.getStatus();
                long area = driverEntity.getArea();
                VehicleResponse vehicleResponse = new VehicleResponse(vehicleEntity.getId(), vehicleEntity.getTotalWeight(), vehicleEntity.getCurrentWeight());
                WarehouseResponse warehouseResponse = new WarehouseResponse();
                WarehouseEntity warehouseEntity = warehouseRepository.getWarehouse(area);

                warehouseResponse.setId(warehouseEntity.getId());
                warehouseResponse.setAreaCode(warehouseEntity.getAreaCode());
                warehouseResponse.setAddress(warehouseEntity.getAddress());
                return ResponseEntity.ok(new DriverResponse(
                        userId,
                        username,
                        userEmail,
                        userPhone,
                        userAddress,
                        jwt,
                        userRole,
                        statusDriver,
                        vehicleResponse,
                        warehouseResponse));
            } else if (userRole.equals("ROLE_STOCKER")){
                StockerEntity stockerEntity = stockerRepository.getStocker(userId);
                WarehouseEntity warehouseEntity = stockerRepository.getWarehouse(stockerEntity.getId());

                WarehouseResponse warehouseResponse = new WarehouseResponse(warehouseEntity.getId(), warehouseEntity.getAddress(), warehouseEntity.getAreaCode());

                return ResponseEntity.ok(new StockerResponse(
                        userId,
                        username,
                        userEmail,
                        userPhone,
                        userAddress,
                        jwt,
                        userRole,
                        warehouseResponse));
            }
            else {
                return ResponseEntity.ok(jwtResponse);
            }
        } catch (Exception e) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Username or Password is incorrect!"));
        }

//        return ResponseEntity.ok(new JwtResponse(
//                userDetails.getId(),
//                userDetails.getUsername(),
//                userDetails.getEmail(),
//                userDetails.getPhone(),
//                userDetails.getAddress(),
//                jwt,
//                userDetails.getRole()));
    }

    @PostMapping("/sign-up")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByPhone(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        String typeRole = "ROLE_USER";
        Long uniqueId = GenID.generateUniqueId();
        // Create new user's account
        UserEntity userEntity = new UserEntity(uniqueId, signUpRequest.getEmail(),
                signUpRequest.getPhone(), signUpRequest.getUsername(),
                signUpRequest.getAddress(),
                encoder.encode(signUpRequest.getPassword()), typeRole, signUpRequest.getProvinceCode());

        //Create new role

        RoleEntity roleEntity = new RoleEntity(uniqueId, typeRole);

        roleRepository.save(roleEntity);
        userRepository.save(userEntity);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PostMapping("/add-driver")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> addDriver(@Valid @RequestBody DriverRequest driverRequest) {

        if (userRepository.existsByPhone(driverRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(driverRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        Long uniqueId = GenID.generateUniqueId();

        // Create new driver's account

        UserEntity userEntity = new UserEntity(uniqueId, driverRequest.getEmail(),
                driverRequest.getPhone(), driverRequest.getUsername(),
                driverRequest.getAddress(),
                encoder.encode(driverRequest.getPassword()), driverRequest.getRole(), "");
        DriverEntity driverEntity = new DriverEntity(uniqueId, driverRequest.getArea(), driverRequest.getVehicleId());

        //Create new role

        RoleEntity roleEntity = new RoleEntity(uniqueId, driverRequest.getRole());

        roleRepository.save(roleEntity);
        userRepository.save(userEntity);
        driverRepository.save(driverEntity);
        log.info("Add driver successfully!");
        return ResponseEntity.ok(new MessageResponse("Add driver successfully!"));
    }

    @PostMapping("/add-vehicle")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> addVehicle(@Valid @RequestBody VehicleRequest vehicleRequest) {

        Long uniqueId = GenID.generateUniqueId();

        VehicleEntity vehicleEntity = new VehicleEntity(uniqueId, vehicleRequest.getTotalWeight());

        vehicleRepository.save(vehicleEntity);
        return ResponseEntity.ok((new MessageResponse("Add vehicle successfully!")));
    }

    @GetMapping("/get-user")
    public ResponseEntity<?> getUser(@Valid @RequestHeader("Authorization") String token) {

        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
            token = token.substring(7, token.length());
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId;
        String username;
        String userEmail;
        String userPhone;
        String userAddress;
        String userRole;
        String userProvinceCode;
        if (principal instanceof UserDetails) {
            userId = ((UserDetailsImpl) principal).getId();
            username = ((UserDetailsImpl) principal).getUsername();
            userEmail = ((UserDetailsImpl) principal).getEmail();
            userPhone = ((UserDetailsImpl) principal).getPhone();
            userAddress = ((UserDetailsImpl) principal).getAddress();
            userRole = ((UserDetailsImpl) principal).getRole();
            userProvinceCode = ((UserDetailsImpl) principal).getProvinceCode();
        } else {
            return ResponseEntity.ok((new MessageResponse("Invalid JWT token")));
        }

        JwtResponse jwtResponse = new JwtResponse(userId,
                username,
                userEmail,
                userPhone,
                userAddress,
                token,
                userRole,
                userProvinceCode);

        if (userRole.equals("ROLE_DRIVER_INTER") || userRole.equals("ROLE_DRIVER_EXTER")) {
            DriverEntity driverEntity = driverRepository.getDriver(userId);
            VehicleEntity vehicleEntity = driverRepository.getVehicle(userId);

            int statusDriver = driverEntity.getStatus();
            long area = driverEntity.getArea();

            VehicleResponse vehicleResponse = new VehicleResponse(vehicleEntity.getId(), vehicleEntity.getTotalWeight(), vehicleEntity.getCurrentWeight());
            WarehouseResponse warehouseResponse = new WarehouseResponse();
            WarehouseEntity warehouseEntity = warehouseRepository.getWarehouse(area);

            warehouseResponse.setId(warehouseEntity.getId());
            warehouseResponse.setAreaCode(warehouseEntity.getAreaCode());
            warehouseResponse.setAddress(warehouseEntity.getAddress());

            return ResponseEntity.ok(new DriverResponse(
                    userId,
                    username,
                    userEmail,
                    userPhone,
                    userAddress,
                    token,
                    userRole,
                    statusDriver,
                    vehicleResponse,
                    warehouseResponse));
        } else if (userRole.equals("ROLE_STOCKER")){
            StockerEntity stockerEntity = stockerRepository.getStocker(userId);
            WarehouseEntity warehouseEntity = stockerRepository.getWarehouse(stockerEntity.getId());

            WarehouseResponse warehouseResponse = new WarehouseResponse(warehouseEntity.getId(), warehouseEntity.getAddress(), warehouseEntity.getAreaCode());

            return ResponseEntity.ok(new StockerResponse(
                    userId,
                    username,
                    userEmail,
                    userPhone,
                    userAddress,
                    token,
                    userRole,
                    warehouseResponse));
        }
        else {
            return ResponseEntity.ok(jwtResponse);
        }
//        System.out.println(" - " + username + "-" + userId + "-" + userEmail + "-" + userPhone);
//        return ResponseEntity.ok(new JwtResponse(
//                userId,
//                username,
//                userEmail,
//                userPhone,
//                userAddress,
//                token,
//                userRole));
    }
    @PostMapping("/add-stocker")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> addStocker(@Valid @RequestBody StockerRequest stockerRequest) {

        if (userRepository.existsByPhone(stockerRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(stockerRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        String typeRole = "ROLE_STOCKER";
        Long uniqueId = GenID.generateUniqueId();

        // Create new stocker's account

        UserEntity userEntity = new UserEntity(uniqueId, stockerRequest.getEmail(),
                stockerRequest.getPhone(), stockerRequest.getUsername(),
                stockerRequest.getAddress(),
                encoder.encode(stockerRequest.getPassword()), typeRole, "");
        StockerEntity stockerEntity = new StockerEntity(uniqueId, stockerRequest.getAreaWarehouseId());

        //Create new role

        RoleEntity roleEntity = new RoleEntity(uniqueId, typeRole);

        roleRepository.save(roleEntity);
        userRepository.save(userEntity);
        stockerRepository.save(stockerEntity);

        return ResponseEntity.ok(new MessageResponse("Add stocker successfully!"));
    }
}
