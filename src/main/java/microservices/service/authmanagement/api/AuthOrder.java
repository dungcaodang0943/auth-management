package microservices.service.authmanagement.api;


import com.google.common.hash.Hashing;
import lombok.extern.log4j.Log4j2;
import microservices.service.authmanagement.entity.*;
import microservices.service.authmanagement.enums.ErrorCode;
import microservices.service.authmanagement.payload.request.GetExternalDriversByStockIdRequest;
import microservices.service.authmanagement.payload.request.UpdateLocationDriverRequest;
import microservices.service.authmanagement.payload.request.UpdateWeightVehicleRequest;
import microservices.service.authmanagement.payload.response.GetExternalDriversByStockIdResponse;
import microservices.service.authmanagement.properties.AppProperties;
import microservices.service.authmanagement.repository.DriverRepository;
import microservices.service.authmanagement.repository.VehicleRepository;
import microservices.service.authmanagement.security.jwt.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("${auth-management.app.entranceDomain}")
public class AuthOrder {

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    EntityManager entityManager;

    @Autowired
    DriverRepository driverRepository;

    @Autowired
    VehicleRepository vehicleRepository;

    @PostMapping("/authen-user")
    public ResponseEntity<ResponseData> authenUser(@Valid @RequestBody AuthenRequest authenRequest){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String message = "Authenticate successfullly!";


        ResponseData responseData = new ResponseData();

        AuthenResponse authenResponse = new AuthenResponse();
        String sigData = authenRequest.getSigData()
                + "|" + appProperties.getWeb().getClientID()
                + "|" + appProperties.getWeb().getHashKey();
        String sig = Hashing.sha256().hashString(sigData, StandardCharsets.UTF_8)
                .toString();
        String typeSig = "sig";
        String typeToken = "token";
        String typeSuccess = "successfullly!";
        String typeFailed = "failed!";
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (sig.equals(authenRequest.getSig())) {
            if (jwtUtil.validateJwtToken(authenRequest.getAccessToken())) {
                String userId = jwtUtil.getUserIdFromJwtToken(authenRequest.getAccessToken());
                responseData = getResponseAuthentication(responseData, authenResponse, typeToken, typeSuccess, Long.parseLong(userId), ErrorCode.SUCCESS.getValue());
            } else {
                responseData = getResponseAuthentication(responseData, authenResponse, typeToken, typeFailed, Long.valueOf(111), ErrorCode.CHECK_TOKEN_NOTMATCH.getValue());
            }
        } else {
            responseData = getResponseAuthentication(responseData, authenResponse, typeSig, typeFailed, Long.valueOf(111), ErrorCode.FAIL.getValue());
        }

        return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
    }

    public static String getMessgeResponeAuthenUser(String typeAuthen, String typeMessage){
        String messgeAuthenUser = "Authenticate" + typeAuthen +  typeMessage;
        return messgeAuthenUser;
    }

    public static ResponseData getResponseAuthentication(ResponseData responseData,AuthenResponse authenResponse,String typeToken,String typeSuccess,Long userId, int errorCode) {
        String message = getMessgeResponeAuthenUser(typeToken, typeSuccess);
        authenResponse.setUserId(userId);
        responseData.setData(authenResponse);
        responseData.setReturnCode(errorCode);
        responseData.setReturnMessage(message);

        return responseData;
    }

    @PostMapping("/get-drivers")
    public ResponseEntity<ResponseData> getDrivers(@Valid @RequestBody GetDriversRequest getDriversRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        GetDriversResponse getDriversResponse = new GetDriversResponse();
        ResponseData responseData = new ResponseData();
        String sigData = getDriversRequest.getReqTime()
                + "|" + appProperties.getWeb().getClientID()
                + "|" + appProperties.getWeb().getHashKey();
        String sig = Hashing.sha256().hashString(sigData, StandardCharsets.UTF_8)
                .toString();
        String message;
        Long stockId = getDriversRequest.getStockId();
        if (sig.equals(getDriversRequest.getSig())) {
            message = "Get drivers successfully!";
            getDriversResponse.setDrivers(queryListDriver(stockId));
            responseData.setData(getDriversResponse);
            responseData.setReturnCode(ErrorCode.SUCCESS.getValue());
            responseData.setReturnMessage(message);
        } else {
            message = "Get drivers unsuccessfully!";
            responseData.setData(getDriversResponse);
            responseData.setReturnCode(ErrorCode.FAIL.getValue());
            responseData.setReturnMessage(message);
        }

        return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
    }

    public List<Driver> queryListDriver(Long stockId) {
        TypedQuery<Object[]> query
                = entityManager.createQuery(
                "SELECT DISTINCT d.id, d.status, v.totalWeight - v.currentWeight as remainWeight FROM DriverEntity d, VehicleEntity v, WarehouseEntity w, UserEntity u WHERE d.vehicleId = v.id and d.status = 1 and d.area = :stockId and v.totalWeight - v.currentWeight > 0 and u.id = d.id and u.role = 'ROLE_DRIVER_INTER' ORDER BY remainWeight DESC", Object[].class);
        query.setParameter("stockId", stockId);
        query.setFirstResult(0);
        query.setMaxResults(5);
        List<Object[]> resultList = query.getResultList();

        List<Driver> drivers = new ArrayList<>();

        for (Object[] row : resultList) {
            log.info("Get driver from order" + row[0]);
            Driver driver = new Driver();
            driver.setDriverId((long) row[0]);
            driver.setStatus((int) row[1]);
            drivers.add(driver);
        }
        return drivers;
    }

    @PostMapping("get-driver-info")
    public ResponseEntity<ResponseData> getDriverInfo(@RequestBody GetDriverInfoRequest getDriverInfoRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        GetDriverInfoResponse getDriverInfoResponse = new GetDriverInfoResponse();
        ResponseData responseData = new ResponseData();
        Driver driver = new Driver();

        DriverEntity driverEntity = driverRepository.getDriver(getDriverInfoRequest.getDriverId());

        if (driverEntity != null) {
            driver.setDriverId(driverEntity.getId());
            driver.setStatus(driverEntity.getStatus());

            getDriverInfoResponse.setDriver(driver);

            responseData.setData(getDriverInfoResponse);
            responseData.setReturnCode(ErrorCode.SUCCESS.getValue());
            responseData.setReturnMessage("Get driver susscessfully!");

            return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
        } else {
            getDriverInfoResponse.setDriver(null);

            responseData.setData(getDriverInfoResponse);
            responseData.setReturnCode(ErrorCode.FAIL.getValue());
            responseData.setReturnMessage("Get driver failed!");

            return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
        }
    }

    @PostMapping("/get-external-drivers-by-stockid")
    public ResponseEntity<ResponseData> getExternalDriversByStockid(@Valid @RequestBody GetExternalDriversByStockIdRequest getExternalDriversByStockIdRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        log.info("getExternalDriversByStockid");
        GetExternalDriversByStockIdResponse getExternalDriversByStockIdResponse = new GetExternalDriversByStockIdResponse();
        ResponseData responseData = new ResponseData();
        String sigData = getExternalDriversByStockIdRequest.getReqTime()
                + "|" + appProperties.getWeb().getClientID()
                + "|" + appProperties.getWeb().getHashKey();
        String sig = Hashing.sha256().hashString(sigData, StandardCharsets.UTF_8)
                .toString();
        String message;
        Long stockId = getExternalDriversByStockIdRequest.getStockId();
        log.info("StockId: " + stockId);
        if (sig.equals(getExternalDriversByStockIdRequest.getSig())) {
            log.info("Get external drivers successfully!");
            message = "Get drivers successfully!";
            getExternalDriversByStockIdResponse.setDrivers(queryListDriverExternal(stockId));
            responseData.setData(getExternalDriversByStockIdResponse);
            responseData.setReturnCode(ErrorCode.SUCCESS.getValue());
            responseData.setReturnMessage(message);
        } else {
            log.info("Get external drivers unsuccessfully!");
            message = "Get drivers unsuccessfully!";
            responseData.setData(getExternalDriversByStockIdResponse);
            responseData.setReturnCode(ErrorCode.FAIL.getValue());
            responseData.setReturnMessage(message);
        }

        return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
    }

    public List<DriverExternal> queryListDriverExternal(Long stockId) {
        log.info("StockID external: " + stockId);
        TypedQuery<Object[]> query
                = entityManager.createQuery(
                "SELECT DISTINCT d.id, d.status, v.totalWeight - v.currentWeight, u.username, u.email, u.phone as remainWeight FROM DriverEntity d, VehicleEntity v, WarehouseEntity w, UserEntity u WHERE d.vehicleId = v.id and d.status = 1 and d.area = :stockId and v.totalWeight - v.currentWeight > 0 and u.id = d.id and u.role = 'ROLE_DRIVER_EXTER' ORDER BY remainWeight DESC", Object[].class);
        query.setParameter("stockId", stockId);
        query.setFirstResult(0);
        query.setMaxResults(5);
        List<Object[]> resultList = query.getResultList();

        List<DriverExternal> drivers = new ArrayList<>();

        for (Object[] row : resultList) {
            log.info("Get external driver from order" + row[0]);
            DriverExternal driverExternal = new DriverExternal();
            driverExternal.setDriverId((long) row[0]);
            driverExternal.setStatus((int) row[1]);
            driverExternal.getTruckInfo().setRemainWeight((float) row[2]);
            driverExternal.setDriverName((String) row[3]);
            driverExternal.setDriverEmail((String) row[4]);
            driverExternal.setPhone((String) row[5]);
            drivers.add(driverExternal);
        }
        return drivers;
    }
    @PostMapping("/get-internal-drivers-by-stockid")
    public ResponseEntity<ResponseData> getInternalDriversByStockid(@Valid @RequestBody GetExternalDriversByStockIdRequest getExternalDriversByStockIdRequest) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        log.info("getInternalDriversByStockid");
        GetExternalDriversByStockIdResponse getExternalDriversByStockIdResponse = new GetExternalDriversByStockIdResponse();
        ResponseData responseData = new ResponseData();
        String sigData = getExternalDriversByStockIdRequest.getReqTime()
                + "|" + appProperties.getWeb().getClientID()
                + "|" + appProperties.getWeb().getHashKey();
        String sig = Hashing.sha256().hashString(sigData, StandardCharsets.UTF_8)
                .toString();
        String message;
        Long stockId = getExternalDriversByStockIdRequest.getStockId();
        if (sig.equals(getExternalDriversByStockIdRequest.getSig())) {
            log.info("Get internal drivers successfully!");
            message = "Get drivers successfully!";
            getExternalDriversByStockIdResponse.setDrivers(queryListDriverInternal(stockId));
            responseData.setData(getExternalDriversByStockIdResponse);
            responseData.setReturnCode(ErrorCode.SUCCESS.getValue());
            responseData.setReturnMessage(message);
        } else {
            log.info("Get internal drivers unsuccessfully!");
            message = "Get drivers unsuccessfully!";
            responseData.setData(getExternalDriversByStockIdResponse);
            responseData.setReturnCode(ErrorCode.FAIL.getValue());
            responseData.setReturnMessage(message);
        }

        return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
    }

    public List<DriverExternal> queryListDriverInternal(Long stockId) {
        log.info("StockID internal: " + stockId);
        TypedQuery<Object[]> query
                = entityManager.createQuery(
                "SELECT DISTINCT d.id, d.status, v.totalWeight - v.currentWeight, u.username, u.email, u.phone as remainWeight FROM DriverEntity d, VehicleEntity v, WarehouseEntity w, UserEntity u WHERE d.vehicleId = v.id and d.status = 1 and d.area = :stockId and v.totalWeight - v.currentWeight > 0 and u.id = d.id and u.role = 'ROLE_DRIVER_INTER' ORDER BY remainWeight DESC", Object[].class);
        query.setParameter("stockId", stockId);
        query.setFirstResult(0);
        query.setMaxResults(5);
        List<Object[]> resultList = query.getResultList();

        List<DriverExternal> drivers = new ArrayList<>();

        for (Object[] row : resultList) {
            log.info("Get internal driver from order" + row[0]);
            DriverExternal driverExternal = new DriverExternal();
            driverExternal.setDriverId((long) row[0]);
            driverExternal.setStatus((int) row[1]);
            driverExternal.getTruckInfo().setRemainWeight((float) row[2]);
            driverExternal.setDriverName((String) row[3]);
            driverExternal.setDriverEmail((String) row[4]);
            driverExternal.setPhone((String) row[5]);
            drivers.add(driverExternal);
        }
        return drivers;
    }

    @PostMapping("/update-weight-vehicle")
    public ResponseEntity<ResponseData> updateWeightVehicle(@Valid @RequestBody UpdateWeightVehicleRequest updateWeightVehicleRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ResponseData responseData = new ResponseData();

        try {
            long driverId = updateWeightVehicleRequest.getDriverId();
            float usedWeight = updateWeightVehicleRequest.getUsedWeight();
            DriverEntity driverEntity = driverRepository.getDriver(driverId);
            long vehicleId = driverEntity.getVehicleId();
            VehicleEntity vehicleEntity = vehicleRepository.getVehicle(vehicleId);
            usedWeight += vehicleEntity.getCurrentWeight();
            vehicleRepository.updateWeight(vehicleId, usedWeight);
            log.info("Update weight successfully!: " + driverId + " " + usedWeight + " " + vehicleId);
            responseData.setData("");
            responseData.setReturnCode(ErrorCode.SUCCESS.getValue());
            responseData.setReturnMessage("Update weight susscessfully!");
        } catch (Exception ex) {
            log.info("Update weight unsuccessfully!");
            responseData.setData("");
            responseData.setReturnCode(ErrorCode.FAIL.getValue());
            responseData.setReturnMessage("Update weight unsusscessfully!");
        }

        return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
    }

    @PostMapping("/update-location-driver")
    public ResponseEntity<ResponseData> updateLocationDriver(@RequestBody UpdateLocationDriverRequest updateLocationDriverRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ResponseData responseData = new ResponseData();

        try {
            long externalDriverId = updateLocationDriverRequest.getExternalDriverId();
            long stockId = updateLocationDriverRequest.getStockId();
            driverRepository.updateLocationDriver(externalDriverId, stockId);
            log.info("Update location driver successfully!: " + externalDriverId + " " + stockId);
            responseData.setData("");
            responseData.setReturnCode(ErrorCode.SUCCESS.getValue());
            responseData.setReturnMessage("Update location driver susscessfully!");
        } catch (Exception ex) {
            responseData.setData("");
            responseData.setReturnCode(ErrorCode.FAIL.getValue());
            responseData.setReturnMessage("Update location driver unsusscessfully!");
            log.info("Update location driver unsusscessfully!");
        }

        return new ResponseEntity<ResponseData>(responseData, headers, HttpStatus.OK);
    }
}
