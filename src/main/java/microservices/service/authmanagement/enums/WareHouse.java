package microservices.service.authmanagement.enums;

import java.util.HashMap;
import java.util.Map;

public enum WareHouse {
    WarehouseReceive(1),
    WarehouseSend(0);

    private static final Map<Integer, WareHouse> ENUM_MAP = new HashMap();

    static {
        for (WareHouse eachEnum : WareHouse.values()) {
            ENUM_MAP.put(eachEnum.getValue(), eachEnum);
        }
    }

    private final int code;

    WareHouse(int code) {
        this.code = code;
    }

    public static WareHouse getEnum(int type) {
        return ENUM_MAP.get(type);
    }

    public int getValue() {
        return code;
    }
}
