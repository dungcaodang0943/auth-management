package microservices.service.authmanagement.enums;

import java.util.HashMap;
import java.util.Map;

public enum ErrorCode {
    SUCCESS(1),
    PARAM_NOT_VALID(2),
    AUTHEN_FAIL(3),
    SERVER_MAINTAINANCE(4),
    CHECK_SIG_NOTMATCH(5),
    CHECK_TOKEN_NOTMATCH(6),
    FAIL(7),
    SAVE_DB_FAIL(8),
    PRODUCE_QUEUE_FAIL(9),
    CALL_API_FAIL(10),
    LOCK_FAIL(11),
    ORDER_STATUS_INVALID(12),
    DRIVER_INACTIVE(13),
    WEIGHT_INVALID(14)
    ;

    private static final Map<Integer, ErrorCode> ENUM_MAP = new HashMap();

    static {
        for (ErrorCode eachEnum : ErrorCode.values()) {
            ENUM_MAP.put(eachEnum.getValue(), eachEnum);
        }
    }

    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public static ErrorCode getEnum(int type) {
        return ENUM_MAP.get(type);
    }

    public int getValue() {
        return code;
    }
}
