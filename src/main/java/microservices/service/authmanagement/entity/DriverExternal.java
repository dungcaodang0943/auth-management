package microservices.service.authmanagement.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DriverExternal {
    private long driverId;
    private int status;
    private String driverName;
    private String driverEmail;
    private String phone;
    private TruckInfo truckInfo = new TruckInfo();

    @Getter
    @Setter
    @NoArgsConstructor
    public class TruckInfo{
        private float remainWeight;
    }

}
