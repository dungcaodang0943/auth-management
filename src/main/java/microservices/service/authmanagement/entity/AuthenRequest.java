package microservices.service.authmanagement.entity;


import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenRequest {

    @SerializedName("accesstoken")
    private String accessToken = "";

    @SerializedName("reqtime")
    private long reqTime = 0;

    @SerializedName("clientid")
    private int clientId = 0;

    private String sig = "";

    public String getSigData() {
        return String.format("%s|%s",
                accessToken,
                reqTime);
    }
}
