package microservices.service.authmanagement.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "warehouse", uniqueConstraints={@UniqueConstraint(columnNames = "address")})
public class WarehouseEntity {
    @Id
    private Long id;

    @NotBlank
    @Size(max = 2)
    private String areaCode;

    @NotBlank
    @Size(max = 200)
    private String address;
}
