package microservices.service.authmanagement.entity;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetDriversRequest {

    @SerializedName("stockid")
    private long stockId = 0;

    @SerializedName("reqtime")
    private long reqTime = 0;

    @SerializedName("clientid")
    private int clientId = 0;

    private String sig = "";
}
