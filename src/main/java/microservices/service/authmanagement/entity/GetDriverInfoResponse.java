package microservices.service.authmanagement.entity;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetDriverInfoResponse {

    private Driver driver;
}
