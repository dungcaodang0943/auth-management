package microservices.service.authmanagement.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table (name = "role")
@NoArgsConstructor
@Getter
@Setter
public class RoleEntity {
    @Getter
    @Setter
    @Id
    private Long userId;

    @Getter
    @Setter
    private String name;

    public RoleEntity(Long userId, String name) {
        this.userId = userId;
        this.name = name;
    }
}


