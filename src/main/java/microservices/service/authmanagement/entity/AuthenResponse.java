package microservices.service.authmanagement.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenResponse {

    @SerializedName("userid")
    private long userId;
}
