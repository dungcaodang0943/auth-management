package microservices.service.authmanagement.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Getter
@Setter
@Entity
@Table(name = "vehicle")
public class VehicleEntity {
    @Id
    private Long id;

    private float totalWeight;

    private float currentWeight = 0;

    public VehicleEntity(Long id, float totalWeight) {
        this.id = id;
        this.totalWeight = totalWeight;
    }

    public VehicleEntity() {
    }
}
