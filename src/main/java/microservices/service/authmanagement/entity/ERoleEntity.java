package microservices.service.authmanagement.entity;

public enum ERoleEntity {
    ROLE_USER,
    ROLE_DRIVER,
}
