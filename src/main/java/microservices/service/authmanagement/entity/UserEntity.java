package microservices.service.authmanagement.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "user", uniqueConstraints={@UniqueConstraint(columnNames = "email"),@UniqueConstraint(columnNames = "phone")})
public class UserEntity {
    @Id
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 11)
    private String phone;

    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 200)
    private String address;

    @NotBlank
    @Size(max = 120)
    private String password;

    private String role;

    private String provinceCode;
}
