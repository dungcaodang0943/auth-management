package microservices.service.authmanagement.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table (name = "driver")
public class DriverEntity {
    @Id
    private Long id;

    private Long area;

    private Long vehicleId;

    private int status = 0;

    public DriverEntity(Long id, Long area, Long vehicleId) {
        this.id = id;
        this.area = area;
        this.vehicleId = vehicleId;
    }

    public DriverEntity() {
    }
}
