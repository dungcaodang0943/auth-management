package microservices.service.authmanagement.repository;


import microservices.service.authmanagement.entity.ERoleEntity;
import microservices.service.authmanagement.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    Optional<RoleEntity> findByName(ERoleEntity name);
}
