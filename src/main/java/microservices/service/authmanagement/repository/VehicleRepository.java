package microservices.service.authmanagement.repository;

import microservices.service.authmanagement.entity.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface VehicleRepository extends JpaRepository<VehicleEntity, Long> {
    @Transactional
    @Modifying
    @Query("update VehicleEntity v set v.currentWeight = :usedWeight" +
            " where v.id = :id")
    void updateWeight(@Param(value = "id") long id,
                            @Param(value = "usedWeight") float usedWeight);

    @Query("select v from VehicleEntity v where v.id = :id")
    VehicleEntity getVehicle(@Param(value = "id") long id);
}
