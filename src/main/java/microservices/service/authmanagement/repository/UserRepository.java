package microservices.service.authmanagement.repository;


import microservices.service.authmanagement.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    Optional<UserEntity> findByEmailOrPhone(String email, String phone);

    Boolean existsByPhone(String phone);

    Boolean existsByEmail(String email);

    @Transactional
    @Modifying
    @Query("update UserEntity u set u.email = :email," +
            " u.phone = :phone," +
            " u.username = :username," +
            " u.address = :address," +
            " u.provinceCode = :provinceCode" +
            " where u.id = :id")
    void updateUserInfo(@Param(value = "id") long id,
                        @Param(value = "email") String email,
                        @Param(value = "phone") String phone,
                        @Param(value = "username") String username,
                        @Param(value = "address") String address,
                        @Param(value = "provinceCode") String provinceCode);
    @Transactional
    @Modifying
    @Query("update UserEntity  u set u.password = :newPassword where u.id = :id")
    void changeUserPassword(Long id, String newPassword);
}
