package microservices.service.authmanagement.repository;

import microservices.service.authmanagement.entity.VehicleEntity;
import microservices.service.authmanagement.entity.WarehouseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WarehouseRepository extends JpaRepository<WarehouseEntity, Long> {
    @Query("select w from WarehouseEntity w where w.areaCode = :addressCode")
    List<WarehouseEntity> getListAddressWarehouse(@Param(value = "addressCode") String addressCode);

    @Query("select w from WarehouseEntity w")
    List<WarehouseEntity> getListWarehouse();

    @Query("select w from WarehouseEntity w where w.id = :id")
    WarehouseEntity getWarehouse(@Param(value = "id") long id);

}
