package microservices.service.authmanagement.repository;

import microservices.service.authmanagement.entity.DriverEntity;
import microservices.service.authmanagement.entity.StockerEntity;
import microservices.service.authmanagement.entity.VehicleEntity;
import microservices.service.authmanagement.entity.WarehouseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StockerRepository extends JpaRepository<StockerEntity, Long> {
    @Query("select s from StockerEntity s where s.id = :id")
    StockerEntity getStocker(@Param(value = "id") long id);

    @Query("select w from StockerEntity s, WarehouseEntity w where s.areaWarehouseId = w.id and s.id = :id")
    WarehouseEntity getWarehouse(@Param(value = "id") long id);
}
