package microservices.service.authmanagement.repository;

import microservices.service.authmanagement.entity.DriverEntity;
import microservices.service.authmanagement.entity.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DriverRepository extends JpaRepository<DriverEntity, Long> {
    @Transactional
    @Modifying
    @Query("update DriverEntity u set u.status = :status" +
            " where u.id = :id")
    void updateStatusDriver(@Param(value = "id") long id,
                        @Param(value = "status") int status);

    @Query("select d from DriverEntity d where d.id = :id")
    DriverEntity getDriver(@Param(value = "id") long id);

    @Query("select v from DriverEntity d, VehicleEntity v where d.vehicleId = v.id and d.id = :id")
    VehicleEntity getVehicle(@Param(value = "id") long id);

//    @Query("select d from DriverEntity d, StockerEntity s where d.area = s.areaWarehouseId and s.id = :id")
//    List<DriverEntity> getDriverEachWarehouse(@Param(value = "id") long id);

    @Transactional
    @Modifying
    @Query("update DriverEntity u set u.area = :stockId" +
            " where u.id = :id")
    void updateLocationDriver(@Param(value = "id") long id,
                            @Param(value = "stockId") long stockId);
}
