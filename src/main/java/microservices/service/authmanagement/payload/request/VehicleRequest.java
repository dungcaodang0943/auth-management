package microservices.service.authmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleRequest {
    private int totalWeight;
}
