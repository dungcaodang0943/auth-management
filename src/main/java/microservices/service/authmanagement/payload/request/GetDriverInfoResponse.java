package microservices.service.authmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;
import microservices.service.authmanagement.entity.Driver;

@Getter
@Setter
public class GetDriverInfoResponse {
    private Driver driver;
}
