package microservices.service.authmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class AddressWarehouseRequest {

    @NotBlank
    @Size(max = 2)
    private String areaCode;

    @NotBlank
    @Size(max = 200)
    private String address;
}
