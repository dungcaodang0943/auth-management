package microservices.service.authmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetTransportationCostRequest {
    private int isDirectlySend;
    private int isDirectlyReceive;
//    private String addressSender;
    private String addressReceiver;
//    private String addressSenderCode;
    private String addressReceiverCode;
    private double weight;
}
