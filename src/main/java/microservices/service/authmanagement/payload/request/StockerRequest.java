package microservices.service.authmanagement.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class StockerRequest {
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 11)
    private String phone;

    @Size(max = 20)
    private String username;

    @NotBlank
    @Size(max = 200)
    private String address;

    @NotBlank
    @Size(max = 120)
    private String password;

    private String areaWarehouseId;
}
