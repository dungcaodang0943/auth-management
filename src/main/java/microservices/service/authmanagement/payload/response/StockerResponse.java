package microservices.service.authmanagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class StockerResponse {
    private Long id;
    private String username;
    private String email;
    private String phone;
    private String address;
    private String token;
    private String type = "Bearer";
    private String role;
    Object warehouseResponse;

    public StockerResponse(Long id, String username, String email, String phone, String address, String token, String role, Object warehouseResponse) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.token = token;
        this.role = role;
        this.warehouseResponse = warehouseResponse;
    }
}
