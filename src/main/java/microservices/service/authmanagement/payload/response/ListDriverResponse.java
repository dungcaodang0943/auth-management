package microservices.service.authmanagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListDriverResponse {
    private Long id;
    private String username;
    private String role;
    private String email;
    private String phone;
    private VehicleResponse vehicleResponse;
    private WarehouseResponse warehouseResponse;
}
