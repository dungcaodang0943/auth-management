package microservices.service.authmanagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import microservices.service.authmanagement.entity.DriverExternal;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetExternalDriversByStockIdResponse {
    List<DriverExternal> drivers;
}
