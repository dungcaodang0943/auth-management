package microservices.service.authmanagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListStockerResponse {
    private long id;
    private String username;
    private String role;
    private String email;
    private WarehouseResponse warehouseResponse;
}
