package microservices.service.authmanagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import microservices.service.authmanagement.entity.DriverEntity;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DriverWarehouseResponse {
    private String username;
    private String phone;
    DriverEntity driverEntity;
}
