package microservices.service.authmanagement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransportationCostResponse {
    private String cost;
    private long srcStockId;
    private long destStockId;
    private String addressSrcWarehouse;
    private String addressDestWarehouse;
    private String message;
}
