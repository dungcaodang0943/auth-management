//package microservices.service.authmanagement.kafka;
//
//import lombok.extern.log4j.Log4j2;
//import microservices.service.authmanagement.entity.DriverEntity;
//import microservices.service.authmanagement.payload.request.UpdateWeightVehicleRequest;
//import microservices.service.authmanagement.repository.DriverRepository;
//import microservices.service.authmanagement.repository.VehicleRepository;
//import microservices.service.authmanagement.utils.GsonUtils;
//import org.apache.kafka.clients.producer.Producer;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//@Log4j2
//@Service
//public class Consumer {
//    @Autowired
//    private KafkaProperties kafkaProperties;
//
//    @Autowired
//    DriverRepository driverRepository;
//
//    @Autowired
//    VehicleRepository vehicleRepository;
//
//    @KafkaListener(topics = "${auth-management.kafka.topic}}", groupId = "group_id")
//    public void consume(String message) throws IOException {
//        log.info(String.format("#### -> Consumed message -> %s", message));
//        UpdateWeightVehicleRequest updateWeightVehicleRequest = GsonUtils.fromJsonString(message, UpdateWeightVehicleRequest.class);
//        long driverId = updateWeightVehicleRequest.getDriverId();
//        float usedWeight = updateWeightVehicleRequest.getUsedWeight();
//        DriverEntity driverEntity = driverRepository.getDriver(driverId);
//        long vehicleId = driverEntity.getVehicleId();
//        vehicleRepository.updateWeight(vehicleId, usedWeight);
//        log.info("Consume kafka topic UPDATE_WEIGHT_VEHICLE: Update weight successfully!");
//    }
//}
