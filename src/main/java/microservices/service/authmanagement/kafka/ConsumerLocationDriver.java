//package microservices.service.authmanagement.kafka;
//
//import lombok.extern.log4j.Log4j2;
//import microservices.service.authmanagement.entity.DriverEntity;
//import microservices.service.authmanagement.payload.request.UpdateLocationDriverRequest;
//import microservices.service.authmanagement.payload.request.UpdateWeightVehicleRequest;
//import microservices.service.authmanagement.repository.DriverRepository;
//import microservices.service.authmanagement.repository.VehicleRepository;
//import microservices.service.authmanagement.utils.GsonUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//
//@Log4j2
//@Service
//public class ConsumerLocationDriver {
//    @Autowired
//    private KafkaProperties kafkaProperties;
//
//    @Autowired
//    DriverRepository driverRepository;
//
//    @Autowired
//    VehicleRepository vehicleRepository;
//
//    @KafkaListener(topics = "${auth-management.kafka.updateLocationDriverTopic}", groupId = "group_id")
//    public void consumerLocationDriver(String message) throws IOException {
//        log.info(String.format("#### -> Consumed message -> %s", message));
//        UpdateLocationDriverRequest updateLocationDriverRequest = GsonUtils.fromJsonString(message, UpdateLocationDriverRequest.class);
//        long externalDriverId = updateLocationDriverRequest.getExternalDriverId();
//        long stockId = updateLocationDriverRequest.getStockId();
//        driverRepository.updateLocationDriver(externalDriverId, stockId);
//        log.info("Consume kafka topic UPDATE_LOCATION_DRIVER: Update weight successfully!");
//    }
//}
