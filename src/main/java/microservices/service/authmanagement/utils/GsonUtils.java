package microservices.service.authmanagement.utils;

import com.google.gson.Gson;

public class GsonUtils {
    public static final Gson gson = new Gson();

    public static String toJsonString(Object obj) {
        return gson.toJson(obj);
    }

    public static <T> T fromJsonString(String sJson, Class<T> t) {
        return gson.fromJson(sJson, t);
    }
}
