package microservices.service.authmanagement.service;

import lombok.Getter;
import lombok.Setter;
import microservices.service.authmanagement.entity.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

@Getter
@Setter
public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String email;

    private String phone;

    private String username;

    private String address;

    private String password;

    private String role;

    private Collection<? extends GrantedAuthority> authorities;

    private String author;

    private String provinceCode;

    public UserDetailsImpl(Long id, String email, String phone, String username, String address, String password, String role, String author, String provinceCode) {
        this.id = id;
        this.email = email;
        this.phone = phone;
        this.username = username;
        this.address = address;
        this.password = password;
        this.role = role;
        this.author = author;
        this.provinceCode = provinceCode;
    }

    public static UserDetailsImpl build(UserEntity userEntity) {

        String author = userEntity.getRole();

        return new UserDetailsImpl(
                userEntity.getId(),
                userEntity.getEmail(),
                userEntity.getPhone(),
                userEntity.getUsername(),
                userEntity.getAddress(),
                userEntity.getPassword(),
                userEntity.getRole(),
                author,
                userEntity.getProvinceCode());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }
}
