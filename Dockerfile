FROM java:8

#FROM maven:alpine
## image layer
#WORKDIR /app/auth-management
## Image layer: with the application
#COPY ./auth-management /app/auth-management
ADD target/auth-management-0.0.1-SNAPSHOT.jar auth-management-0.0.1-SNAPSHOT.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","auth-management-0.0.1-SNAPSHOT.jar"]